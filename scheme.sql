--MySQL--

create table users(id int primary key auto_increment,
                   first_name text not null,
                   last_name text not null,
                   birthday date not null,
                   age int not null
);

create table emails(user_id int not null,
                    email text,
                    foreign key (user_id) references users(id) on delete cascade
);

create table movies(id int primary key auto_increment,
                    name text not null,
                    director text not null,
                    release_date date,
                    genre text
);

create table tv_shows(id int primary key auto_increment,
                      name text not null,
                      director text not null,
                      release_date date,
                      genre text
);

create table seasons(id int primary key auto_increment,
                     number int not null,
                     name text,
                     tv_show_id int not null,
                     foreign key (tv_show_id) references tv_shows(id) on delete cascade
);

create table episodes(id int primary key auto_increment,
                      name text,
                      number int not null,
                      duration int,
                      season_id int not null,
                      foreign key (season_id) references seasons(id) on delete cascade
);

create table movies_rated(user_id int not null,
                          movie_id int not null,
                          rating int not null,
                          foreign key (user_id) references users(id) on delete cascade,
                          foreign key (movie_id) references movies(id) on delete cascade
);

create table tv_shows_rated(user_id int not null,
                            tv_show_id int not null,
                            rating int not null,
                            foreign key (user_id) references users(id) on delete cascade,
                            foreign key (tv_show_id) references tv_shows(id) on delete cascade
);